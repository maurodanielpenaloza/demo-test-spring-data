package com.guru.demotest.persona;

import com.guru.demotest.ObjectUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* PersonaService
*/
@Service
public class PersonaService {
    
    @Autowired
    private PersonaRepository entityRepository;

	public Persona save(PersonaConsume entity) {
        if(entity != null) {
            return entityRepository.save(ObjectUtils.convertValue(entity, Persona.class));
        } else {
            throw new IllegalArgumentException("La entidad a persistir no puede ser nula");
        }
	}
}