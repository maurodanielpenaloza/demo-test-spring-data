package com.guru.demotest.persona;

import org.springframework.data.repository.CrudRepository;

/**
* PersonaRepository
*/
public interface PersonaRepository extends CrudRepository<Persona, Long> {

}