package com.guru.demotest.persona;

import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonGetter;

/**
 * PersonaConsume
 */
public class PersonaConsume {
    @NotBlank(message = "{name.blank}")
    private String name;
    @NotBlank(message = "{lastname.blank}")
    private String lastname;
    @NotNull(message = "{age.null}")
    @Min(value = 18, message = "{age.min}")
    @Max(value = 70, message = "{age.max}")
    private Integer age;
    @NotNull(message = "{birthdate.null}")
    private Date birthdate;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }
}