package com.guru.demotest.persona;

import com.guru.demotest.ObjectUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


/**
* PersonaResource
*/
@RestController
@RequestMapping("/persona")
public class PersonaResource {
    
    @Autowired
    private PersonaService entityService;

    @PostMapping(value="")
    public PersonaResponse postMethodName(@Validated @RequestBody PersonaConsume entity) {
        Persona persona = entityService.save(entity);
        return ObjectUtils.convertValue(persona, PersonaResponse.class);
    }
    

}