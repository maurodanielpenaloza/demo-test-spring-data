package com.guru.demotest;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
/**
 * ObjectUtils
 */
public class ObjectUtils {

      public static ObjectMapper mapper = new ObjectMapper();

      public static <T> T convertValue(Object from, Class<T> toClass) throws RuntimeException {
          try {
              mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
              return mapper.convertValue(from, toClass);
          } catch (Exception e) {
              e.printStackTrace();
              if (from != null) {
                  throw new RuntimeException("Error al intentar mappear un objeto de la clase " + from.getClass() + "a la clase " + toClass, e);
              } else {
                  throw new RuntimeException("Error al intentar mappear un objeto a la clase " + toClass, e);
              }
          }
      }
  
  
  
      public static String convertValue(Object from) throws RuntimeException {
          try {
              return mapper.writeValueAsString(from);
          } catch (Exception e) {
              e.printStackTrace();
              throw new RuntimeException("Error al intentar mappear un objeto de la clase " + from.getClass() + "a JSON", e);
  
          }
      }
  
      public static <T> List<T> convertValue(List from, Class<T> toClass) throws RuntimeException {
          try {
              List<T> list = new ArrayList<>();
              for (Object element : from) {
  
                  list.add(convertValue(element, toClass));
              }
              return list;
          } catch (Exception e) {
              throw new RuntimeException("Error al intentar mappear un objeto de la clase " + from.getClass() + "a la clase " + toClass, e);
          }
      }
}