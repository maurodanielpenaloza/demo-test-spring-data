package com.guru.demotest;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.HashMap;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        basePackages = {
                "com.guru"
        },
        entityManagerFactoryRef = "guruTestDbEntityManager",
        transactionManagerRef = "guruTestDbTransactionManager"
)
public class JPAConfig {
    

    @Bean
    public DataSource testDataSource() {
            return new EmbeddedDatabaseBuilder()
                    .setType(EmbeddedDatabaseType.H2)
                    .setName("testTestDB")
                    .continueOnError(false)
                    .build();
    }

    //EntityManager.
    @Bean(name = "guruTestDbEntityManager")
    public LocalContainerEntityManagerFactoryBean testEntityManager(DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean em
                = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan(new String[]{"com.guru"});
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setPersistenceUnitName("embeddedTestEM");
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", "create");
        properties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.put("hibernate.hbm2ddl.import_files", "import.sql");
        em.setJpaPropertyMap(properties);

        return em;
    }

    //TransactionManager.
    @Bean(name = "guruTestDbTransactionManager")
    public PlatformTransactionManager guruTestDbTransactionManager(@Qualifier("guruTestDbEntityManager") final LocalContainerEntityManagerFactoryBean guruTestDbEntityManagerFactory) {
        return new JpaTransactionManager(guruTestDbEntityManagerFactory.getObject());
    }

}