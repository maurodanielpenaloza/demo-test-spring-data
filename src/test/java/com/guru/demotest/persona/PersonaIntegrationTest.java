package com.guru.demotest.persona;

import com.guru.demotest.JPAConfig;
import com.guru.demotest.ObjectUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import java.nio.charset.StandardCharsets;


import static org.assertj.core.api.Assertions.assertThat;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * PersonaIntegrationTest
 */
@RunWith(SpringJUnit4ClassRunner.class)//<-- 1. Correr el test con esa clase
@Import({JPAConfig.class, PersonaService.class})//<-- 2. Importar la clase JPAConfig para la configuracion de los repositorios y la clase de logica de negocio
@WebMvcTest(PersonaResource.class) //<-- Aca se incluye el controlador donde estan los metodos rest que se necesitan testear
public class PersonaIntegrationTest {

    @Autowired
    private MockMvc mvc; // <-- Bean de utilidad de spring para poder ejecutar servicios rest
    // Revisar archivo en carpeta --> src/test/resources
    @Value("classpath:PersonaConsume.json") // <-- Archivo donde esta el json que se va a mandar a persistir
    Resource personaConsumeJson;

    @Autowired
    PersonaRepository personaRespository; // <-- Repositorio apra comprobar que el registro se creo correctamente

    /**
     * TESTEO EL METODO POST... QUE REGISTRA UNA NUEVA PERSONA
     * @throws Exception
     */

    @Test
    public void personaTest_Alta() throws Exception {
        //1. Creao el objeto Consume para revisar que los datos se conviertan bien desde el json
        PersonaConsume personaConsume = ObjectUtils.mapper.readValue(personaConsumeJson.getFile(), PersonaConsume.class);
        assertThat(personaConsume.getName()).isNotNull().isNotEmpty();
        assertThat(personaConsume.getAge()).isNotNull();
        assertThat(personaConsume.getLastname()).isNotNull().isNotEmpty();
        assertThat(personaConsume.getBirthdate()).isNotNull();
        //2. Configuro al cliente REST para ejecutar el metodo post
        this.mvc.perform(post("/persona")
            .contextPath("")
            .content(ObjectUtils.convertValue(personaConsume))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk()) //3. <-- Reviso que el estado que retorna el servicio fue un 200
            .andExpect((result) -> {
                //4. Comienzo a testear el resultado que retorno el servicio
                String jsonResponse = result.getResponse().getContentAsString(StandardCharsets.UTF_8);
                PersonaResponse personaResponse = ObjectUtils.mapper.readValue(jsonResponse, PersonaResponse.class);
                assertThat(personaResponse).isNotNull();
                assertThat(personaResponse.getId()).isNotNull();
                Persona personaInDataBase = personaRespository.findById(personaResponse.getId())
                        .orElseThrow(() -> new RuntimeException("No existe la persona en la base de datos"));
                assertThat(personaInDataBase.getAge()).isEqualTo(personaResponse.getAge()).isEqualTo(personaConsume.getAge());
                assertThat(personaInDataBase.getName()).isEqualTo(personaResponse.getName()).isEqualTo(personaConsume.getName());
                assertThat(personaInDataBase.getLastname()).isEqualTo(personaResponse.getLastname()).isEqualTo(personaConsume.getLastname());
                assertThat(personaInDataBase.getBirthdate().getTime()).isEqualTo(personaResponse.getBirthdate().getTime()).isEqualTo(personaConsume.getBirthdate().getTime());
            });
    }
    
}