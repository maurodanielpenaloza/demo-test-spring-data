package com.guru.demotest.persona;

import static org.mockito.ArgumentMatchers.anyObject;

import java.util.Date;

import com.guru.demotest.ObjectUtils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import static org.assertj.core.api.Assertions.assertThat;
/**
 * PersonaUnitTest
 * Clase de ejemplo para el uso de Mockito y su utilidad para crear tests unitarios
 */
@RunWith(SpringJUnit4ClassRunner.class)//<-- 1. Correr el test con esa clase
@Import(PersonaService.class)//<-- 2. Importar la clase donde esta el codigo que se desea testear (pueden ser varias)
public class PersonaUnitTest {

    @MockBean
    PersonaRepository personaRepository; //<-- 3. Mockear todos los beans que necesite la/las clase/s del punto 2

    @Autowired
    PersonaService personaService;//<-- 4. Inyectar los beans del punto 2 para testearlos

    //5. Comenzar a escribir sus tests unitarios.... esta todo listo para que te TESTIES HASTA LOS TOBILLOS
    /**
     * TESTEO DE FUNCIONALIDAD EN CASO DE ERROR
     * Este test es para probar que realmente se libere la excepcion cuando le enviamos un dato nulo
     */
    @Test(expected = IllegalArgumentException.class)
    public void savePersona_expectedError() {
        personaService.save(null);
    }
    /**
     * TESTEO DE FUNCIONALIDAD
     * este test es para probar el correcto funcionamiento del mejor de los casos
     * el caso en que sale todo bien.
     */
    @Test
    public void savePersona() {
        PersonaConsume consume = makeConsume(); //<-- 1. Construyo el dato de entrada
        Persona persona =  ObjectUtils.convertValue(consume, Persona.class);//<-- 2. Construyo el dato de salido del mock, en este caso el repositorio
        persona.setId(1L);
        // 3. Le indico a mockito que cuando se llame a ese metodo me retorne el dato que construi para el en el punto 2
        Mockito.when(personaRepository.save(ArgumentMatchers.any(Persona.class))).thenReturn(persona); 

        Persona p = personaService.save(consume); //<-- 4. Testeo la funcionalidad

        PersonaResponse response =  ObjectUtils.convertValue(p, PersonaResponse.class); //<-- 5. comienzo a revisar que todo este correcto.
        assertThat(response).isNotNull();
        assertThat(response.getId()).isEqualTo(persona.getId());
    }

    private PersonaConsume makeConsume() {
        PersonaConsume consume = new PersonaConsume();
        consume.setAge(27);
        consume.setName("Mauro");
        consume.setLastname("Peñaloza");
        consume.setBirthdate(new Date());
        return consume;
    }

}